# Étapes possibles pour ce lab

Allez chercher sqlite3.exe si vous êtes sur Windows.

Il se trouve dans la page de download de sqlite3 dans un fichier zip

Téléchargez le .zip comportant le nom sqlite-tools

(Optionnel) Ajoutez sqlite3 au PATH de son Windows

Google est votre ami ! D'autres personnes ont déjà essayé d'installer sqlite3 et de l'utiliser. Vous y arriverez aussi !

Créez sa base de données grâce à sqlite3

Placez le fichier musique.sql dans votre dossier de travail du lab

Si vous n'arrivez pas à placer sqlite3.exe dans votre path, placez sqlite3.exe dans ce même dossier

Dans votre ligne de commande, tapez :
`sqlite3 musique.db`

Suite à cette commande, **vous rentrez dans la console de sqlite3**.

sqlite3 attend la suite, si vous tapez `.exit` pour sortir maintenant, votre BD ne sera pas créée

Vous devriez voir l'inscription `sqlite >` à gauche de votre ligne de commande

Tapez `.read musique.sql` afin que sqlite3 exécute le script bâti par Jacques

Tapez `.exit`afin de sortir de la console sqlite3

Créez votre code python et faites le rouler comme au premier lab.

**Vous pouvez voir des exemples de code qui s'arrête là où la plupart d'entre vous étiez arrivés aujourd'hui dans le reste du dépôt**

# Bonne chance et à lundi prochain !
