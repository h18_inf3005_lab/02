#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlite3

#On se connecte à la DB
connection = sqlite3.connect('musique.db')
cursor = connection.cursor()
#On prend tous les artistes
cursor.execute("select * from artiste")
for row in cursor:
    #On sépare la rangée en variables faciles à utiliser
    #Considérez ceci comme le fait de split() une ligne d'un fichier
    identifier, nom, est_solo, combien = row
    print("Artiste: %d et nom: %s" % (identifier, nom))

    #On demande à l'usager de rentrer le choix de l'ID de l'artiste dont on veut afficher les albums
    print("Choisissez un artiste en rentrant son numéro")
    #On le convertit en int afin qu'il corresponde à l'int inscrit dans la primary key identifiant la rangée de l'artiste
    choix = int(input())

    #À FAIRE
    #On positionne le curseur sur la table des albums
    #Il y a moyen de n'aller chercher que les artistes qui comportent l'ID demandé par l'usager
    #Le mot clé est WHERE. Vous pouvez en voir des exemples ici :
    #https://docs.python.org/3/library/sqlite3.html
    cursor.execute("select * from album")
    for row in cursor:
        #On sépare la rangée en variables, comme plus haut
        identifier, album, year, artiste_id, publisher_id = row
        #On affiche l'artiste correspondant au choix de l'usager
        if (choix == artiste_id):
            print("%s, %d" % (album, year))

            #On ouvre le fichier d'input
            #Comment peut-on attraper les potentielles erreurs dûes à un fichier absent?
            #https://docs.python.org/3/tutorial/errors.html
            with open('input.txt', 'r') as infile:
                for line in infile:
                    splitted_line = line.split('|');
                    #Voici comment accéder à un élément d'une ligne "éclatée" (splitée)
                    # print(splitted_line[0], splitted_line[1], splitted_line[2])
                    cursor.execute("select * from artiste")
                    artiste_existe = False
                    current_artiste_id = 0

                    #À FAIRE
                    #Une boucle qui permet de vérifier si l'artiste existe
                    #On devra récupérer ici un numéro d'artiste afin de le réutiliser plus bas lors de l'insertion d'un album
                    #Si l'artiste n'existe pas, nous devons ajouter un artiste à la fin de la table
                    #Aidez-vous de l'exemple de Jacques disponible ici:
                    #https://github.com/jacquesberger/exemplesINF3005/blob/master/SQLite/insert-update.py

                    #Pour information, voici une manière NON SÉCURITAIRE d'insérer une ligne d'un artiste
                    #Comment le feriez-vous de manière sécuritaire ?
                    # if (not(artiste_existe)):
                    # cursor.execute(("insert into artiste(nom, est_solo, nombre_individus) "
                    # 	" values('%s', 0, 0)") % (splitted_line[0]))
                    # connection.commit()

                    #Remarquez que cette commande sqlite est une string. Pour qu'elle puisse s'étaler sur plusieurs lignes
                    #Nous la faisons se terminer par des doubles guillemets et faisons démarrer la prochaine ligne
                    #par ces mêmes guillemets

                    #À FAIRE
                    #Une boucle qui permet d'ajouter chaque album dans la table avec le bon numéro d'artiste crééé plus haut
                    #Peut-être que dans votre boucle plus haut, vous pouvez récupérer le bon numéro d'artiste...
                    #Il faut aussi vérifier si l'album existe afin de ne pas polluer votre table avec des doublons

                    #Au final, tout ceci crée beaucoup de connexions avec la DB.
                    #Comment faire en sorte de réduire le nombre de ces connexions ?
connection.close()
